# cgit-config

A "backup" of the [cgit][] configuration on [cgit.linuxgemini.space][]

You _probably_ can use this config with minimal modification, though I can't give you a guarantee for that.

---

The site currently uses [cgit v1.2.3][], which is manually compiled to link Lua 5.3.

Lua scripts `email-*.lua` requires the OpenSSL Lua module. In Ubuntu, it is `lua-luaossl`.

You can truncate the contents in `/usr/share/cgit/head.html`, the analytics code utilizes CORS so it won't work on any other domains.

The Python script `syntax-highlighting.py` requires Python 3 and the `Pygments` module.

Although `cheddar` is unused; when activated, it requires the environment variable `BAT_SYNTAXES`, which is set to `/opt/bat/cache/syntaxes.bin` with the profile.d file `/etc/profile.d/02-bat.sh`.

If you are hosting [cgit][] behind Cloudflare, ***please disable Email Obfuscation*** on your cgit (sub)domain, either via Page Rules (like `cgit.linuxgemini.space/*` as the URL) or globally disabling it on your domain. Not doing so will break syntax highlighting on random places like "blame".

## Additional Info

Although this is just a config repository, there are people to credit to.

Please check the `CREDITS.md` file for more information.

[cgit]: https://git.zx2c4.com/cgit/about/
[cgit.linuxgemini.space]: https://cgit.linuxgemini.space/
[cgit v1.2.3]: https://git.zx2c4.com/cgit/tag/?h=v1.2.3
