# Credits

[cgit][] is primarily developed by [Jason A. Donenfeld][] ([zx2c4][]).

The provided build of [cheddar][] in this repository is written by [Vincent Ambo][], available at [code.tvl.fyi][].

[SWI-Prolog Sublime Syntax][] is written/provided by [Benjamin Schaaf][].

[cgit]: https://git.zx2c4.com/cgit/about/
[Jason A. Donenfeld]: https://www.jasondonenfeld.com/
[zx2c4]: https://www.zx2c4.com/
[cheddar]: https://code.tvl.fyi/tree/tools/cheddar
[Vincent Ambo]: https://tazj.in/
[code.tvl.fyi]: https://code.tvl.fyi/
[SWI-Prolog Sublime Syntax]: https://github.com/BenjaminSchaaf/swi-prolog-sublime-syntax
[Benjamin Schaaf]: https://github.com/BenjaminSchaaf
