#
# cgit config
# see cgitrc(5) for details

# Enable caching of up to 1000 output entries
#cache-size=1000

# Cache time to live 
#cache-dynamic-ttl=5
#cache-repo-ttl=5

# Specify the css url
css=/cgit.css

# Show owner on index page
enable-index-owner=1

# Allow http transport git clone
enable-http-clone=1
clone-url=https://$HTTP_HOST/$CGIT_REPO_URL

# Show extra links for each repository on the index page
enable-index-links=1

# Let users follow links on log
enable-follow-links=1

# Enable git blame
enable-blame=1

# Enable ASCII art commit history graph on the log pages
enable-commit-graph=1

# Show number of affected files per commit on the log pages
enable-log-filecount=1

# Show number of added/removed lines per commit on the log pages
enable-log-linecount=1

# Sort branches by date
branch-sort=age

# Add a cgit favicon
favicon=/favicon.ico

# Use a (custom) logo
#logo=
logo=/cgit.png

#max-stats=week

# Set the title and heading of the repository index page
root-title=warehouse

# Set a subheading for the repository index page
root-desc=linuxgemini's usual spot to dump random or small things

# Include some more info about example.com on the index page
#root-readme=/usr/share/cgit/readme.html

# Allow download of tar.gz, tar.bz2 and zip-files
#snapshots=tar.gz tar.bz2 zip

##
## List of common mimetypes
##

mimetype.gif=image/gif
mimetype.html=text/html
mimetype.jpg=image/jpeg
mimetype.jpeg=image/jpeg
mimetype.pdf=application/pdf
mimetype.png=image/png
mimetype.svg=image/svg+xml

mimetype-file=/etc/mime.types

# Highlight source code with python pygments-based highligher
source-filter=/usr/lib/cgit/filters/syntax-highlighting.py
#source-filter=/usr/lib/cgit/filters/cheddar

# Format markdown, restructuredtext, manpages, text files, and html files
# through the right converters
about-filter=/usr/lib/cgit/filters/about-formatting.sh
#about-filter=/usr/lib/cgit/filters/cheddar-about

# avatars!
email-filter=lua:/usr/lib/cgit/filters/email-libravatar-proxy6.lua

# owner link test
owner-filter=lua:/usr/lib/cgit/filters/owner-beta.lua

##
## Search for these files in the root of the default branch of repositories
## for coming up with the about page:
##
readme=:README.md
readme=:readme.md
readme=:README.mkd
readme=:readme.mkd
readme=:README.rst
readme=:readme.rst
readme=:README.html
readme=:readme.html
readme=:README.htm
readme=:readme.htm
readme=:README.txt
readme=:readme.txt
readme=:README
readme=:readme
readme=:INSTALL.md
readme=:install.md
readme=:INSTALL.mkd
readme=:install.mkd
readme=:INSTALL.rst
readme=:install.rst
readme=:INSTALL.html
readme=:install.html
readme=:INSTALL.htm
readme=:install.htm
readme=:INSTALL.txt
readme=:install.txt
readme=:INSTALL
readme=:install

# Change the root base
virtual-root=/

# <head> includes, headers and footers
head-include=/usr/share/cgit/head.html
#header=/usr/share/cgit/header.html
#footer=/usr/share/cgit/footer.html

# Source gitweb.description, gitweb.owner from each project config
enable-git-config=1

# Enable serving HTML on /plain
#enable-html-serving=1

# Remove .git suffix from project display
remove-suffix=1

# Add robots rules
robots=noindex,nofollow,noarchive,nosnippet,notranslate,noimageindex

# Git projects list, pointed to gitolite
project-list=/var/lib/gitolite3/projects.list
scan-path=/var/lib/gitolite3/repositories
