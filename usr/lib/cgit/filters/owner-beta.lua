-- This script is an example of an owner-filter.  It replaces the
-- usual query link with one to a fictional homepage.  This script may
-- be used with the owner-filter or repo.owner-filter settings in
-- cgitrc with the `lua:` prefix.

function filter_open()
	buffer = ""
end

function filter_close()
	if buffer == "linuxgemini" then
		link = "https://linuxgemini.space/"
	elseif buffer == "a" then
		link = "https://ave.zone/"
	elseif buffer == "reswitched" then
		link = "https://reswitched.team/"
	else
		link = "http://example.com/"
	end

	html(string.format("<a href=\"%s\">%s</a>", link, buffer))
	return 0
end

function filter_write(str)
	buffer = buffer .. str
end
